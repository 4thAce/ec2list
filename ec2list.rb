class Ec2Instances
  require 'fog/aws'
  if File.exists?('/home/rmagahiz')
    PATH='/home/rmagahiz'
  elsif File.exists?('/Users/richardmagahiz')
    PATH='/Users/richardmagahiz'
  else
    exit(-1)
  end

  def grabInstances(account,region)
    ec2=Hash.new
    ary=Array.new
    f = File.open("#{PATH}/aws-#{account}-access")
    accesskey = f.read.chomp
    f.close
    g = File.open("#{PATH}/aws-#{account}-sec")
    sec = g.read.chomp
    g.close
    ec2[account] = Hash.new
    ec2[account][region]=Fog::Compute.new :provider => 'AWS',
                     :region => region,
                     :aws_access_key_id => accesskey,
                     :aws_secret_access_key => sec
    ec2[account][region].describe_instances.body['reservationSet'].each do |instance|
      isobject = instance['instancesSet'].each do |anobject|
        ary.push( {'dnsName' => anobject['dnsName'],
         'name' => anobject['tagSet']['Name'],
         'instanceType' => anobject['instanceType'],
         'instanceId' => anobject['instanceId'],
         'privateIpAddress' => anobject['privateIpAddress'],
         'ipAddress' => anobject['ipAddress'],
         'virtualizationType' => anobject['virtualizationType'],
         'instanceState' => anobject['instanceState']['name'],
        })
      end
    end
    return ary.sort! { |first, second| first['name'].to_s <=> second['name'].to_s}
  end

end
