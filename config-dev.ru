require 'rubygems'
require 'sinatra'
require 'bundler'
Bundler.setup

set :environment, :development
set :port, 8000
disable :run, :reload

root = File.expand_path File.dirname(__FILE__)
require File.join( root, './ec2list.rb')
require File.join( root, './sinatraec2.rb')
require File.join( root, './app/config.rb')

map "/" do
  run Ec2WebApp
end
