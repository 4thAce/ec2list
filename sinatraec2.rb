require 'sinatra'
require 'fog/aws'
require_relative './ec2list'
require 'tilt/erb'

class Ec2WebApp < Sinatra::Application
  
  get '/' do
    fog = Ec2Instances.new
    @grabbed=Hash.new
    %W(prod dev).each do |account|
      @grabbed[account]=Hash.new
      %W(us-west-1 us-west-2 us-east-1).each do |region|
        @grabbed[account][region] = fog.grabInstances(account,region)
      end
    end
    #puts @grabbed.to_s
    erb :index
  end

end
