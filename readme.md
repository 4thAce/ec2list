This is a Sinatra app which gives tabular lists of AWS instances, for prod and for dev, in each of the three
AWS regions we work with. For each instance, the app prints the name, the state (running or stopped), the public IP if any,
the private IP, the instance ID, and the instance type. The idea is that this makes it easy for a user to search for an
IP address to find where the instance is placed, or search for an Amazon instance name to find out the instance ID,
or whatever.

This is currently running at instances.4dlib.com. Refreshing the page tells the app to re-fetch the info from AWS
(this is not done automatically but is controlled by the user's browser only)