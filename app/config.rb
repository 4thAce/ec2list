module Ec2List

  if ENV["RACK_ENV"] == "development"
         require 'pry'
         require 'pry-nav'
  end

  ONE_MONTH  = 60 * 60 * 24 * 30
  
  def self.app
    Rack::Builder.app do

      run Ec2WebApp
    end
  end
end
